# -*- coding:utf-8 -*-
import configparser
from Config.conf import cm


class ReadConfig(object):
    """配置文件"""
    def __init__ (self):
        self.config = configparser.RawConfigParser()# 当有%的符号时请使用Raw读取
        self.config.read(cm.ini_file, encoding= 'utf-8')

    def _get(self, section, option):
        """获取"""
        return self.config.get(section, option)
    def _set(self, section, option, value):
        """更新"""
        self.config.set(section, option, value)
        with open(cm.ini_file, 'w') as f:
            self.config.write(f)
    @property
    def url(self):
        return self._get(self.env, 'base_url')

    @property
    def env(self):
        return self._get('ENV','env')

    def get_value(self,option:None):
        return self._get(self.env, option)

ini = ReadConfig()

if __name__ == '__main__':
    print(ini.url)
    print((ini._get(ini._get('ENV','env'),'username')))
    print((ini._get(ini._get('ENV', 'env'), 'password')))
    print(ini.env)
    print(ini.get_value('password'))