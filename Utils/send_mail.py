import zmail

from Common.exchange_data import ExchangeData
from Config.conf import cm
from Utils.logger import log
import os
import zipfile
class Mail(object):
    def send_report(self):
        """发送报告"""
        log.info('开始将allure报告压缩zip包')
        file_path = os.path.join(cm.REPORT_DIR, 'allure')
        Mail.zip_file(
            file_path=file_path, out_path=os.path.join(cm.ALLURE_REPORT_ZIP_DIR, 'allure.zip'))
        log.info('压缩打包allure报告完成')
        allure_zip_report = os.path.join(cm.ALLURE_REPORT_ZIP_DIR, 'allure.zip')
        pytest_report = os.path.join(cm.REPORT_DIR, 'pytest', 'report.html')
        try:
            mail = {
                'from': '15316036798@163.com',
                'subject': '最新的测试报告邮件',
                'content_html': ExchangeData.get_pytest_summary(),
                'attachments': [pytest_report, allure_zip_report]
            }
            server = zmail.server(*cm.EMAIL_INFO.values())
            server.send_mail(cm.ADDRESSEE, mail)
            log.info("邮件发送成功！")
        except Exception as e:
            log.info("Error: 无法发送邮件，{}！".format(e))

    def zip_file(file_path: str, out_path: str):
        """
        压缩指定文件夹
        :param file_path: 目标文件夹路径
        :param out_path: 压缩文件保存路径+xxxx.zip
        :return: 无
        """
        zip_path = cm.ALLURE_REPORT_ZIP_DIR
        if not os.path.exists(zip_path):
            os.mkdir(zip_path)
        out_path = os.path.join(zip_path, 'allure.zip')
        zip = zipfile.ZipFile(out_path, "w", zipfile.ZIP_DEFLATED)
        for path, dirnames, filenames in os.walk(file_path):
            # 去掉目标跟路径，只对目标文件夹下边的文件及文件夹进行压缩
            fpath = path.replace(file_path, '')
            for filename in filenames:
                zip.write(
                    os.path.join(
                        path, filename), os.path.join(
                        fpath, filename))
        zip.close()

if __name__ == "__main__":
    '''请先在Config/conf.py文件设置邮箱的账号和密码'''
    Mail().send_report()

