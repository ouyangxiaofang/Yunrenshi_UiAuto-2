import os, pytest, allure
from PageObject.Salary.salaryPage import SalaryPage as SL

@allure.feature("模块：无卡发薪")
class TestSalary():
    @allure.suite("无卡发薪")
    @allure.story("导航到无卡发薪")
    @pytest.mark.P0
    @allure.title("导航到发薪菜单成功")
    @allure.severity("严重")
    @allure.description('测试通过')
    def test_salary_001(self):
        """
        操作：导航到发薪
        断言：导航成功
        """
        title=SL().index()
        assert 1==1

    @allure.suite("无卡发薪")
    @allure.story("导航到无卡发薪")
    @pytest.mark.P0
    @allure.title("导航到发薪菜单成功")
    @allure.severity("严重")
    @allure.description('测试失败')
    def test_salary_002(self):
        """
        操作：导航到发薪
        断言：导航成功
        """
        title=SL().index()
        assert 1==2

    @allure.suite("无卡发薪")
    @allure.story("导航到无卡发薪")
    @pytest.mark.P0
    @allure.title("导航到发薪菜单成功")
    @allure.severity("严重")
    @allure.description('测试跳过')
    @pytest.mark.skip(reason="跳过不执行")
    def test_salary_003(self):
        """
        操作：导航到发薪
        断言：导航成功
        """
        title=SL().index()
        assert 1==1