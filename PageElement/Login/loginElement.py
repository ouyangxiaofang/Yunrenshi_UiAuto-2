# -*- coding:UTF-8 -*-
from selenium.webdriver.common.by import By
class LoginElement:
    """
    示例页面的元素定位
    """
    url="/login"
    #用户名
    username = (By.CSS_SELECTOR, "input[name='username']")
    #密码
    password = (By.CSS_SELECTOR, "input#password")
    #登录按钮
    btn = (By.CSS_SELECTOR, ".el-button.login-btn-span")
    #登录成功后企业名称
    company_name=(By.CSS_SELECTOR,".nav-bar-company")



