# -*- coding:utf-8 -*-
import os
from selenium.webdriver.common.by import By
from Utils.times import dt_strftime

class ConfigManager(object):
    # 项目名称
    project_name="云人事"
    # 项目目录
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # 页面元素目录
    ELEMENT_PATH = os.path.join(BASE_DIR,'PageElement')
    # 测试数据目录
    TEST_DATA_DIR = os.path.join(BASE_DIR, "TestData")
    # 测试用例目录
    TEST_CASE_DIR = os.path.join(BASE_DIR, "TestCase")
    #截图路径
    SCREEN_DIR = os.path.join(BASE_DIR, "ScreenShots",'{}'.format(dt_strftime("%Y-%m-%d")))
    #测试报告路径
    REPORT_DIR = os.path.join(BASE_DIR, "Report")
    #allure测试报告路径
    ALLURE_REPORT_DIR=os.path.join(REPORT_DIR, "allure")
    # allure测试报告压缩路径
    ALLURE_REPORT_ZIP_DIR = os.path.join(REPORT_DIR,'allure_zip')
    # pytest测试报告路径
    PYTEST_REPORT_DIR=os.path.join(REPORT_DIR, "pytest")
    #发送测试报告样式目录
    CONTENT_HTML=os.path.join(BASE_DIR,"Config","report.html")
    # 元素定位的类型
    LOCATE_MODE = {
        'css': By.CSS_SELECTOR,
        'xpath': By.XPATH,
        'name': By.NAME,
        'id': By.ID,
        'class': By.CLASS_NAME
    }
    # 邮件信息
    EMAIL_INFO = {
        'username': '15316036798@163.com',
        'password': 'OY892020',
        'smtp_host': 'smtp.163.com',
        'smtp_port': 465
    }
    # 收件人
    ADDRESSEE = ['2995016124@qq.com','892020877@qq.com']
    @property
    def log_file(self):
        """日志目录"""
        log_dir = os.path.join(self.BASE_DIR, 'Logs','{}'.format(dt_strftime("%Y-%m-%d")))
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        return os.path.join(log_dir, '{}.log'.format(dt_strftime("%Y_%m_%d_%H_%M_%S")))

    @property
    def reports_file(self):
        """测试报告目录"""
        reports_dir = os.path.join(self.BASE_DIR, 'Reports','{}'.format(dt_strftime("%Y-%m-%d")))
        if not os.path.exists(reports_dir):
            os.makedirs(reports_dir)

    @property
    def screen_file(self):
        """截图目录"""
        screen_dir = self.SCREEN_DIR
        run_dir = os.path.join(self.SCREEN_DIR,'run')
        run_error_dir = os.path.join(self.SCREEN_DIR, 'run_error')
        if not os.path.exists(screen_dir):
            os.makedirs(screen_dir)
        if not os.path.exists(run_dir):
            os.makedirs(run_dir)
        if not os.path.exists(run_error_dir):
            os.makedirs(run_error_dir)
        return screen_dir,run_dir,run_error_dir
    @property
    def ini_file(self):
        """配置文件"""
        ini_file = os.path.join(self.BASE_DIR, 'Config', 'config.ini')
        if not os.path.exists(ini_file):
            raise FileNotFoundError("配置文件%s不存在！" % ini_file)
        return ini_file

cm = ConfigManager()
if __name__ == '__main__':
    print(cm.BASE_DIR)
    print(cm.log_file)
    print(cm.screen_file)
    print(cm.screen_file[2])
    print(cm.LOCATE_MODE.get('css'))
