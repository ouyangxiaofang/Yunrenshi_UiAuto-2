# -*- coding:UTF-8 -*-
import time
import datetime
from functools import wraps
import calendar as cal


def timestamp():
    """时间戳"""
    return time.time()
def dt_strftime(fmt="%Y_%m_%d_%H_%M_%S"):
    """
        datetime格式化时间
        :param fmt "%Y%m%d %H%M%S
    """
    return datetime.datetime.now().strftime(fmt)
def sleep(seconds=1.0):
    """睡眠时间"""
    time.sleep(seconds)
def running_time(func):
    """函数运行时间"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = timestamp()
        res = func(*args, **kwargs)
        print("校验元素done！用时%.3f秒！"% (timestamp() - start))
        return res
    return wrapper

# 获取当月的第一天和最后一天
def get_first_and_last_day():
    i = datetime.datetime.now()
    FORMAT = "%d-%d-%d\t%d-%d-%d"
    d = cal.monthrange(i.year, i.month)
    list = str(FORMAT % (i.year, i.month, 1, i.year, i.month, d[1])).split("\t")
    list = "-".join(list).split("-")
    for count in range(len(list)):
        if len(list[count]) == 1:
            list[count] = "0" + str(list[count])
            print(list[count])
    list = "-".join(list)
    return list

# 距离今天N天的未来某一天
def future_oneday(countdays):
    today = time.time()
    future_five = today + countdays * 86400
    future_one_date = time.strftime('%Y-%m-%d', time.localtime(future_five))
    return future_one_date

if __name__ == '__main__':
    print(timestamp())
    print(dt_strftime("%Y%m%d%H%M%S"))
    print(dt_strftime("%Y-%m-%d"))
    print(dt_strftime())
    print(get_first_and_last_day())
    print(future_oneday(1))





