# -*- coding:UTF-8 -*-
import base64
import os
import pytest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from Common.exchange_data import ExchangeData
from Config.conf import cm
from Utils.logger import log
from Utils.times import *
from PageObject.Login.loginPage import LoginPage
import allure

driver=LoginPage().login()
# @pytest.fixture，框架提供的能力。将access_web()打上注解，供别人做初始化的调用。
# scope="session"，多个文件调用一次
@pytest.fixture(scope="session")
def access_web():
    # 前置：打开浏览器
    # 修改页面加载策略
    desired_capabilities = DesiredCapabilities.CHROME
    # 注释这两行会导致最后输出结果的延迟，即等待页面加载完成再输出
    desired_capabilities["pageLoadStrategy"] = "none"
    global driver
    if driver is None:
        # 实例化对象
        driver = webdriver.Chrome()
        driver.maximize_window()
    # 返回对象
    yield driver
    # 后置：关闭浏览器
    driver.quit()

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item,call):
    """
    当测试失败的时候，自动截图，展示到html报告中
    :param item:
    """
    outcome = yield
    report = outcome.get_result()
    report.description = str(item.function.__doc__)
    pytest_html = item.config.pluginmanager.getplugin('html')
    report.description = str(item.function.__doc__)
    extra = getattr(report, 'extra', [])
    if (report.when == 'call' or report.when == "setup"):
        xfail = hasattr(report, 'wasxfail')
    if (report.skipped and xfail or (report.failed and not xfail)):
        screen_img=_capture_screenshot()
        html = '<div><img src="data:image/png;base64,%s" alt="screenshot" style="width:600px;height:300px;" ' \
                       'οnclick="window.open(this.src)" align="right"/></div>' % screen_img
        extra.append(pytest_html.extras.html(html))
    report.extra = extra

def _capture_screenshot():
    """截图保存为base64"""
    sleep(1)
    screen_file = os.path.join(cm.screen_file[2],'失败截图_{}.png'.format(dt_strftime('%Y%m%d%H%M%S')))
    driver.save_screenshot(screen_file)
    sleep(1)
    allure.attach.file(screen_file, "失败截图", allure.attachment_type.PNG)
    with open(screen_file, 'rb')  as f:
        imagebase64 = base64.b64encode(f.read())
    return imagebase64.decode()

def pytest_configure(config):
    config.addinivalue_line("markers", 'smoke')
    config.addinivalue_line("markers", 'P0')
    config.addinivalue_line("markers", 'P1')
    config.addinivalue_line("markers", 'P2')
    config.addinivalue_line("markers", 'P3')

def pytest_terminal_summary(terminalreporter):
    """
    收集测试结果
    """
    _PASSED = len([i for i in terminalreporter.stats.get('passed', []) if i.when != 'teardown'])
    _ERROR = len([i for i in terminalreporter.stats.get('error', []) if i.when != 'teardown'])
    _FAILED = len([i for i in terminalreporter.stats.get('failed', []) if i.when != 'teardown'])
    _SKIPPED = len([i for i in terminalreporter.stats.get('skipped', []) if i.when != 'teardown'])
    _TOTAL = terminalreporter._numcollected
    _TIMES = time.time() - terminalreporter._sessionstarttime
    log.info(f"用例总数: {_TOTAL}")
    log.info(f"通过用例: {_PASSED}")
    log.info(f"异常用例数: {_ERROR}")
    log.info(f"失败用例数: {_FAILED}")
    log.info(f"跳过用例数: {_SKIPPED}")
    log.info(f"用例执行时长: {round(_TIMES, 2)} s")
    try:
        _RATE = _PASSED / _TOTAL * 100

        _SUCCESS_RATE=round(_RATE, 2)

    except ZeroDivisionError:
        _SUCCESS_RATE="0.00"
    log.info(f"用例成功率:{_SUCCESS_RATE}")
    result_data_test={
        "_TOTAL": f"{_TOTAL}",
        '_PASSED':f"{_PASSED}",
        "_ERROR": f" {_ERROR}",
        "_FAILED": f" {_FAILED}",
        "_SKIPPED": f" {_SKIPPED}",
        "_TIMES": f"{round(_TIMES, 2)} s",
        "_SUCCESS_RATE": f"{_SUCCESS_RATE}",
    }
    ExchangeData.post_pytest_summary(result_data_test)#测试结果添加到变量池
    with open("result.txt", "w") as fp:#测试结果保存到本地result.txt
        fp.write("_TOTAL=%s" % _TOTAL + "\n")
        fp.write("_PASSED=%s" % _PASSED + "\n")
        fp.write("_FAILED=%s" % _FAILED + "\n")
        fp.write("_ERROR=%s" % _ERROR + "\n")
        fp.write("_SKIPPED=%s" % _SKIPPED + "\n")
        fp.write("_SUCCESS_RATE=%.2f%%" % _SUCCESS_RATE + "\n")
        fp.write("_TIMES=%.2fs" % _TIMES)

@pytest.fixture(scope='function',autouse=True)
#用例执行开始与结束标志
def start_end():
    log.info("{:=^100s}".format("执行测试用例"))
    yield
    log.info("{:=^100s}".format("用例执行结束"))

